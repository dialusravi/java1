package com.dialus.dao;

import com.dilaus.model.Login;
import com.dilaus.model.User1;

public interface UserDao {
	  void register(User1 user);
	  User1 validateUser(Login login);
	}